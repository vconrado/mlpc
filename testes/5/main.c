// vim: set ts=8
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mlp.h"
#include "mlp_topology.h"
#include "matrix.h"
#include "defines.h"

float f_ativacao(float v){
   return (1./(1.+exp(-v)));
}

int
main(int argc, char **argv)
{
        mlp_stats_t *stats = create_mlp_stats();

        mlp_topology_t *topology;
        topology = create_mlp_topology(2, 3, 1, f_ativacao);
        
        matrix_t *X = load_matrix("../X");
        matrix_t *D = load_matrix("../D");
        
        mlp_training_param_t *parameter; 
        parameter = create_mlp_training_param(X, D, NULL, NULL, 0.02, 10000000, 0.3, 0.7, 1);
        
        mlp_arch_t *architecture;
        architecture = training_mlp_basic(topology, parameter, stats);
        mlp_training_param_print(parameter);
        mlp_stats_print(stats);
        mlp_topology_print(topology);
        fprint_mlp_arch(stdout, architecture);
        
        save_mlp_arch("", architecture);
        
        return 0;
}
