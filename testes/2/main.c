#include <stdio.h>
#include <math.h>

#include "mlp.h"
#include "matrix.h"


static __inline__ float f_ativacao(float v){
   return (1./(1.+exp(-v)));
}

int main(){
   size_t n_escondidos = 2;
   float eta = .2;
   float alpha = .7;
   float tolerancia_erro = .00001;
   size_t max_epocas = 100000;

   matrix_t *X = load_matrix("../X");
   matrix_t *D = load_matrix("../D");

   print_matrix(X);
   print_matrix(D);
   
   mlp_t *mlp = create_mlp(n_escondidos, eta, alpha, tolerancia_erro, max_epocas, f_ativacao);

   int final = training_mlp(&mlp, X, D, 1);
   
   printf("%s\n", get_short_log(mlp));
   
   arch_mlp_t *arch = get_arch_mlp(mlp);
   
   if(final == 1){
      printf("Treinada com %u epocas e %.10f de erro acumulado.\n",arch->epoca_final, arch->erro_acc);
   }else{
      printf("Treinamento finalizado pelo limite de epocas (%u) com erro %.10f.\n", arch->epoca_final, arch->erro_acc);
   }
   
   print_arch_mlp(arch);
   
   printf("Saida: \n");
   matrix_t *S = execute_mlp(arch, X, f_ativacao);
   print_matrix(S);
   
   if(save_arch_mlp(arch, "porta_or")){
      fprintf(stderr,"<main> nao foi possivel salvar arch.\n");
   }
   return 0;
}
