// vim: set ts=8
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mlp.h"
#include "mlp_topology.h"
#include "mlp_arch.h"
#include "matrix.h"

float f_ativacao(float v){
   return (1./(1.+exp(-v)));
}

int
main(int argc, char **argv)
{
        mlp_topology_t *topology;
        mlp_arch_t *architecture;
        matrix_t *V;
        
        matrix_t *X = load_matrix("X.mat");
        matrix_t *W1 = load_matrix("W1.mat");
        matrix_t *W2 = load_matrix("W2.mat");
        matrix_t *B1 = load_matrix("B1.mat");
        matrix_t *B2 = load_matrix("B2.mat");
        
        if (X == NULL || W1 == NULL || W2 == NULL || B1 == NULL || B2 == NULL) {
                fprintf(stderr, "<main> NULL. (%p, %p, %p, %p, %p)\n", X, W1, W2, B1, B2);
                return 1;
        }
        
        size_t n_input = get_cols_matrix(W1);
        size_t n_hidden = get_rows_matrix(W1);
        size_t n_output = get_rows_matrix(W2);

        topology = create_mlp_topology(n_input, n_hidden, n_output, f_ativacao);
        architecture = create_mlp_arch();
        mlp_arch_set(architecture, W1, W2, B1, B2);
        
        V = execute_mlp(topology, architecture, X);
         
        fprint_mlp_arch(stdout, architecture);
        printf("V:\n");
        print_matrix(V);
        return 0;
}
