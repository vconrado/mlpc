#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../src/matrix.h"
#include "../src/mlp.h"
float f_ativacao(float v){
   return (1./(1.+exp(-v)));
}

int main(int argc, char **argv){
   size_t n_escondidos = 2; // numero de neuronios da camada escondida
   size_t n_entrada; // numero de neuronios de entrada (verificado em X)
   size_t n_saida; // numero neuronios saida (verificado em D)
   size_t n_padroes; // numero de padroes (verificado em X e D)
   size_t epoca;
   size_t padrao;
   float eta = 0.2; // taxa de aprendizagem
   float alpha = .7; // momentum
   float tolerancia_erro = 0.0001; // erro maximo toleravel
   float erro, erro_acc;
   float dw1, dw2, db1, db2;
   size_t max_epocas = 1000000; // numero maximo de epocas
   float (*funcao_ativacao)(float) = NULL; // funcao de ativacao
   funcao_ativacao = f_ativacao;
   float acc;
   size_t r, c;
   
   
   matrix_t *X; // padroes
   matrix_t *D; // valores desejados de saida
   matrix_t *W1, *W2, *B1, *B2; // pesos, e bias
   matrix_t *Y1, *Y2, *E, *GS, *G1; // saidas da camada escondida, saida da camada de saida, erro, gradiente da saida, gradiente da camada escondia
   matrix_t *DW1A, *DB1A, *DW2A, *DB2A; // valores antigos de w1,w2,b1 e b2
   // lendo matrix de padroes e valores desejados
   X = load_matrix("X");
   if(X == NULL){
      fprintf(stderr, "Nao lido X.\n");
      return EXIT_FAILURE;
   }
   D = load_matrix("D");
   if(D == NULL){
      fprintf(stderr, "Nao lido D.\n");
      destroy_matrix(D);
      return EXIT_FAILURE;
   }
   
   // pegando informacoes do problema
   n_entrada = get_rows_matrix(X);
   n_saida = get_rows_matrix(D);
   n_padroes = get_cols_matrix(X);
   
   printf("Identificados: %d entradas, %d neuronios escondidos, %d saidas e %d padroes.\n", n_entrada, n_escondidos, n_saida, n_padroes);
   if(n_padroes != get_cols_matrix(D)){
      fprintf(stderr, "Numero de padroes diferentes entre X e D.\n");
      destroy_matrix(D);
      destroy_matrix(X);
      return EXIT_FAILURE;
   }
   
   printf("D\n");
   print_matrix(D);
   
   // criando matrizes de pesos e bias
   W1 = create_matrix(n_escondidos, n_entrada);
   fill_matrix_randonly(&W1,1);
   DW1A = create_matrix(n_escondidos, n_entrada);
   fill_matrix_zero(&DW1A);
   
   B1 = create_matrix(n_escondidos, 1);
   fill_matrix_randonly(&B1,1);
   DB1A = create_matrix(n_escondidos, 1);
   fill_matrix_zero(&DB1A);
   
   
   W2 = create_matrix(n_saida, n_escondidos);
   fill_matrix_randonly(&W2,1);
   DW2A = create_matrix(n_saida, n_escondidos);
   fill_matrix_zero(&DW2A);
   
   
   B2 = create_matrix(n_saida, 1);
   fill_matrix_randonly(&B2,1);
   DB2A = create_matrix(n_saida, 1);
   fill_matrix_zero(&DB2A);
   
   Y1 = create_matrix(n_escondidos, 1);
   Y2 = create_matrix(n_saida, 1);
   E = create_matrix(n_saida, 1);
   GS = create_matrix(n_saida, 1);
   G1 = create_matrix(n_escondidos, 1);
   
   printf("W1\n");
   print_matrix(W1);
   printf("B1\n");
   print_matrix(B1);
   printf("W2\n");
   print_matrix(W2);
   printf("B2\n");
   print_matrix(B2);
   
   printf("X\n");
   print_matrix(X);
   /* Iniciando treinamento */
   printf("Iniciando...\n");
   /* Para cada epoca */
   for(epoca = 0; epoca<max_epocas; epoca++){
      /* Para cada padrao*/
      erro_acc = 0.;
      for(padrao=0; padrao<n_padroes; padrao++){
         /* Calcula atividade interna da camada escondida */
         for(r = 0; r<n_escondidos; r++){
            acc = -1.*B1->storage[r][0];
            for(c = 0; c<n_entrada; c++){
               acc = acc + W1->storage[r][c] * X->storage[c][padrao];
            }
            /* calcula funcao de ativacao */
            Y1->storage[r][0] = funcao_ativacao(acc);
         }
         /* Calcula atividade interna da camada de saida */
         for(r = 0; r<n_saida; r++){
            acc = -1.*B2->storage[r][0];
            for(c = 0; c<n_escondidos; c++){
               acc = acc + W2->storage[r][c] * Y1->storage[c][0];
            }
            /* calcula funcao de ativacao */
            Y2->storage[r][0] = funcao_ativacao(acc);
         }
         #ifdef DEBUG
         printf("\n\n### Padrao %u ########################\n", padrao);
         printf("Y1\n");
         print_matrix(Y1);
         printf("Y2\n");
         print_matrix(Y2);
         #endif
         /* Calcula o erro e o gradiente da camada de saida */
         for(r = 0; r<n_saida; r++){
            erro = (D->storage[r][padrao] - Y2->storage[r][0]);
            erro_acc = erro_acc + (erro*erro);
            #ifdef DEBUG
            printf("Erro %f - %f =: %.10f\n",D->storage[r][padrao], Y2->storage[r][0], erro);
            #endif
            GS->storage[r][0] = erro * Y2->storage[r][0]*(1.-Y2->storage[r][0]);
         }
         /* Calculando Gradiente da camada escondida */
         for(r = 0; r<n_escondidos; r++){
            acc = 0.;
            for(c = 0; c<n_saida; c++){
               acc = W2->storage[c][r] * GS->storage[c][0];
            }
            G1->storage[r][0] = Y1->storage[r][0]*(1.-Y1->storage[r][0])*acc;
         }
         #ifdef DEBUG
         printf("GS\n");
         print_matrix(GS);
         printf("G1\n");
         print_matrix(G1);
         #endif
         /* Deltas e atualiza Ws e Bs*/
         for(r = 0; r<n_escondidos; r++){
            for(c=0; c<n_entrada; c++){
               dw1 = eta*G1->storage[r][0]*X->storage[c][padrao];
               W1->storage[r][c] = W1->storage[r][c] + dw1 + alpha*DW1A->storage[r][c];
               DW1A->storage[r][c] = dw1;
            }
            db1 = eta*G1->storage[r][0];
            B1->storage[r][0] = B1->storage[r][0] - db1 - alpha*DB1A->storage[r][0];
            DB1A->storage[r][0] = db1;
         }
         #ifdef DEBUG
         printf("DW1\n");
         print_matrix(DW1A);
         printf("DB1\n");
         print_matrix(DB1A);
         #endif
         for(r = 0; r<n_saida; r++){
            for(c = 0; c<n_escondidos; c++){
               dw2 = eta*GS->storage[r][0]*Y1->storage[c][0];
               W2->storage[r][c] = W2->storage[r][c] + dw2 + alpha*DW2A->storage[r][c];
               DW2A->storage[r][c] = dw2;
            }
            db2 = eta*GS->storage[r][0];
            B2->storage[r][0] = B2->storage[r][0] - db2 - alpha*DB2A->storage[r][0];
            DB2A->storage[r][0] = db2;
         }
         #ifdef DEBUG
         printf("DW2\n");
         print_matrix(DW2A);
         printf("DB2\n");
         print_matrix(DB2A);
         #endif

         #ifdef DEBUG
         printf("W1\n");
         print_matrix(W1);
         printf("B1\n");
         print_matrix(B1);
         printf("W2\n");
         print_matrix(W2);
         printf("B2\n");
         print_matrix(B2);
         #endif
      } // fim dos padroes
/*      if(epoca == 2){*/
/*         break;*/
/*      }*/
      if(erro_acc <= tolerancia_erro){
         printf("Deu: %f Epoca: %d\n", erro_acc, epoca);
         break;
      }else{
/*         if(epoca%500 == 0){*/
/*            printf("Epoca %d, Erro_acc = %f\n",epoca, erro_acc);*/
/*         }*/
      }
   } // fim das epocas
   
   printf("X\n");
   print_matrix(X);
   printf("D\n");
   print_matrix(D);
   printf("W1\n");
   print_matrix(W1);
   printf("B1\n");
   print_matrix(B1);
   printf("W2\n");
   print_matrix(W2);
   printf("B2\n");
   print_matrix(B2);
   
   printf("Testando Rede E=%f:\n",erro_acc);
   for(padrao=0; padrao<n_padroes; padrao++){
      /* Calcula atividade interna da camada escondida */
      for(r = 0; r<n_escondidos; r++){
         acc = -1.*B1->storage[r][0];
         for(c = 0; c<n_entrada; c++){
            acc = acc + W1->storage[r][c] * X->storage[c][padrao];
         }
         /* calcula funcao de ativacao */
         Y1->storage[r][0] = funcao_ativacao(acc);
      }
      /* Calcula atividade interna da camada de saida */
      printf("Y2[%u] = ",padrao);
      for(r = 0; r<n_saida; r++){
         acc = -1.*B2->storage[r][0];
         for(c = 0; c<n_escondidos; c++){
            acc = acc + W2->storage[r][c] * Y1->storage[c][0];
         }
         /* calcula funcao de ativacao */
         Y2->storage[r][0] = funcao_ativacao(acc);
         printf("%.10f ", Y2->storage[r][0]);
      }
      printf("\n");
      
   }
   
   
/*   destroy_matrix(X);*/
/*   destroy_matrix(D);*/
/*   destroy_matrix(W1);*/
/*   destroy_matrix(DW1A);*/
/*   destroy_matrix(B1);*/
/*   destroy_matrix(DB1A);*/
/*   destroy_matrix(W2);*/
/*   destroy_matrix(DW2A);*/
/*   destroy_matrix(B2); */
/*   destroy_matrix(DB2A);*/
/*   destroy_matrix(Y1);*/
/*   destroy_matrix(Y2);*/
/*   destroy_matrix(E);*/
/*   destroy_matrix(GS);*/
/*   destroy_matrix(G1);*/
   
   
   mlp_t *mlp = create_mlp(n_escondidos, eta, alpha, tolerancia_erro, max_epocas, f_ativacao);
   print_mlp(mlp);
   return 0;
}
