// vim: set ts=8
#include <stdio.h>
#include <math.h>
#include "mlp.h"
#include "mlp_topology.h"
#include "mlptrainer_defines.h"
#include "mlptrainer_argp.h"

float f_ativacao(float v){
   return (1./(1.+exp(-v)));
}

int 
main (int argc, char **argv)
{
        // parse arguments
        arguments_t *arguments;
        arguments = mlptrainer_argp_parse(argc, argv, 0, 0);
        
        
        // for quiet executions
        if (arguments->quiet == 1) {
                fclose(stdout);
                fclose(stderr);
        }

        // show options if in verbose mode
        if (arguments->verbose == 1) {
                mlptrainer_argp_print(arguments);
        }

        // Create stats struct if necessary
        mlp_stats_t *stats = NULL;
        if (arguments->statistics == 1) {
                stats = create_mlp_stats();
         }


        // Load data from files
        matrix_t *X = load_matrix(arguments->input_file);
        if (X == NULL) {
                fprintf(stderr,"<mlp_trainer> Nao foi possivel abrir arquivo %s.\n",arguments->input_file);
                return 1;
        }
        matrix_t *D = load_matrix(arguments->desired_file);
        if (D == NULL) {
                fprintf(stderr,"<mlptrainer> Nao foi possivel abrir arquivo %s.\n",arguments->desired_file);
                destroy_matrix(X);
                return 1;
        }
        if (arguments->verbose == 1) { 
		printf("X:\n");
                print_matrix(X);
		printf("D:\n");
                print_matrix(D);
        }
        // Get and Test the sizes of matrixes
        size_t n_input = X->rows;
        
        size_t n_output = D->rows;
        
        if (X->cols != D->cols) {
                fprintf(stderr, "<mlptrainer> Total de colunas das matrizes X e D nao conferem (%d, %d).\n", X->cols, D->cols);
                destroy_matrix(X);
                destroy_matrix(D);
                return 1;
        }
        
        // Creating and defining MLP Topology
        mlp_topology_t *topology;
        topology = create_mlp_topology(n_input, arguments->n_hidden, n_output, f_ativacao);
        mlp_topology_print(topology);
       
        // Creating and defining MLP Parameters
        mlp_training_param_t *parameter; 
        parameter = create_mlp_training_param(X, D, NULL, NULL, arguments->max_error, 
                                        arguments->m_epoch, arguments->eta, arguments->alpha, 1);
       
       
        // Training MLP
        mlp_arch_t *architecture;
        architecture = training_mlp_basic(topology, parameter, stats);
        
        
        fprint_mlp_arch(stdout, architecture);
        
        // write architecture
        save_mlp_arch(arguments->prefix, architecture);

        if (arguments->statistics == 1) {
                mlp_training_param_print(parameter);
                mlp_stats_print(stats);
                mlp_topology_print(topology);
        }

        destroy_mlp_training_param(parameter);
        destroy_matrix(X);
        destroy_matrix(D);
        return 0;
}
