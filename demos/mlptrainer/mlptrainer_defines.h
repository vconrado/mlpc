// vim: set ts=8
#ifndef __MLP_TRAINER_DEFINES_H__
#define __MLP_TRAINER_DEFINES_H__

#define N_HIDDEN_DEFAULT 2
#define N_HIDDEN_DEFAULT_S "2"

#define N_MAX_EPOCHS 1000000
#define N_MAX_EPOCHS_S "10^6"

#define N_ETA (0.3)
#define N_ETA_S "0.3"

#define N_ALPHA (0.7)
#define N_ALPHA_S "0.7"
#endif /* __MLP_TRAINER_DEFINES_H__ */


