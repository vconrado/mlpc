// vim: set ts=8
#ifndef __MLPARGUMENTS_H__
#define __MLPARGUMENTS_H__

#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <string.h> //atoi atof
#include "mlptrainer_defines.h"

typedef struct arguments {
        int quiet;
        int verbose;
        // mlptrainer options:
        char *desired_file;
        float max_error;
        float generalization;
        unsigned int n_hidden;
        char *input_file;
        unsigned int m_epoch; // max epochs
        char *prefix;
        int randomize;
        int statistics;
        char *test_file;
        float eta;
        float alpha;
}arguments_t;

arguments_t *
mlptrainer_argp_parse(int argc, char **argv, unsigned int flags, int *arg_index);

void
mlptrainer_argp_print(arguments_t *arguments);

#endif /* __MLPARGUMENTS_H__ */

