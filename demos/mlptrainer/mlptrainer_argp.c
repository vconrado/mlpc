// vim: set ts=8
#include "mlptrainer_argp.h"


/*  *********************************************************
        Variaveis usadas para o help e parser do mlptrainer 
    *********************************************************  */


const char *argp_program_version = "mlptrainer 0.1";
const char *argp_program_bug_address ="<mlptrainer-bug@vconrado.com>";

// documentacao
static char doc[] = 
        "Train a MLP Neural Network.\
        \v\
        Example:\n \
        mlptrainer -h 10 -g 20 input_data_set.mat desired_data_set.mlp";

// argumentos 
static char args_doc[] = "input_data_file desired_data_file";

// argumentos que sao aceitados
static struct argp_option options[] = {
        {"verbose",     'v',    0,              0,              "Produce verbose output (debug)" },
        {"debug",       0,      0,              OPTION_ALIAS},
        {"quiet",       'q',    0,              0,              "Don't produce any output" },
/*        {"silent",      's',    0,              OPTION_ALIAS},*/
        {0,             0,      0,              0,              "MLP architecture options:"},
        {"prefix",      'p',    "STRING",       0,              "Output prefix (STRING_)"},
        {"hidden",      'h',    "COUNT",        0,              "Number of neurons in the hidden layer (default="N_HIDDEN_DEFAULT_S")"},
/*        {"input",       'i',    "FILE",         0,              "File with input data set (X)"},*/
        {0,             'x',    0,              OPTION_ALIAS},
/*        {"desired",     'd',    "FILE",         0,              "File with disered data set (D)"},*/
        {"generalization",'g',  "COUNT",        0,              "Use COUNT \% of inputs for generalizations tests (default=0) \n**Total error calculated with this input set"},
        {"test",        't',    "FILE",         0,              "File with test data set (T)"},
        {"randomize",   'r',    0,              0,              "Randomize input data before start the train"},
        {"error",       'e',    "COUNT",        0,              "Stops training when this error is reached (default=0)"},
        {"epoch",       'm',    "COUNT",        0,              "Trains up to COUNT epochs (default="N_MAX_EPOCHS_S")"},
        {"statistics",  's',    0,              0,              "Show statistics"},
        {"eta",         'n',    "COUNT",        0,              "Learing rate - eta (default="N_ETA_S""},
        {"momentum",    'a',    "COUNT",        0,              "Momentum - alpha (default="N_ALPHA_S")"},
        { 0 }
};

// faz o tratamento de cada argumento passado
static error_t
mlptrainer_parse_opt (int key, char *arg, struct argp_state *state)
{
        // pega a estrutura para atualizar
        struct arguments *arguments = state->input;

        switch (key) {
                case 'q':
                        arguments->quiet = 1;
                        break;
                case 'v':
                        arguments->verbose = 1;
                        break;
                case 'd':
                        arguments->desired_file = arg;
                        break;
                case 'e':
                        arguments->max_error = atof(arg);
                        break;
                case 'g':
                        arguments->generalization = atof(arg);
                        break;
                case 'h':
                        arguments->n_hidden = atoi(arg);
                        break;
                case 'i':
                case 'x':
                        arguments->input_file = arg;
                        break;
                case 'm':
                        arguments->m_epoch = atoi(arg);
                        break;
                case 'p':
                        arguments->prefix = arg;
                        break;
                case 'r':
                        arguments->randomize = 1;
                        break;
                case 's':
                        arguments->statistics = 1;
                        break;
                case 't':
                        arguments->test_file = arg;
                        break;
                case 'n':
                        arguments->eta = atof(arg);
                        break;
                case 'a':
                        arguments->alpha = atof(arg);
                        break;
                case ARGP_KEY_ARG:
                        switch (state->arg_num ) {
                                case 0:
                                        arguments->input_file = arg;
                                        break;
                                case 1: 
                                        arguments->desired_file = arg;
                                        break;
                                default:
                                        // excesso de argumentos
                                        argp_usage(state);
                        }
                        break;
                case ARGP_KEY_END:
                        if (state->arg_num < 2) {
                                argp_usage(state);
                        }
                        break;
                default:
                        return ARGP_ERR_UNKNOWN;
        
        }
        return 0;
}


// funcao que faz o tratamento e o registro para o parser
arguments_t *
mlptrainer_argp_parse(int argc, char **argv, unsigned int flags, int *arg_index)
{
        // variavel enviada ao parser
        static struct argp argp = { options, mlptrainer_parse_opt, args_doc, doc };

        arguments_t *arguments = (arguments_t *) malloc(sizeof(arguments_t));

        /* Default values. */
        arguments->quiet = 0;
        arguments->verbose = 0;
        // mlp architecture options:
        arguments->desired_file = NULL;
        arguments->max_error = 0;
        arguments->generalization = 0;
        arguments->n_hidden = N_HIDDEN_DEFAULT;
        arguments->input_file = NULL;
        arguments->m_epoch = N_MAX_EPOCHS;
        arguments->prefix = NULL;
        arguments->randomize = 0;
        arguments->statistics = 0;
        arguments->test_file = NULL;
        arguments->eta = N_ETA;
        arguments->alpha = N_ALPHA;
        // call parser
        argp_parse (&argp, argc, argv, flags, arg_index, arguments);

        return arguments;
}


void
mlptrainer_argp_print(arguments_t *arguments)
{
        printf("Global options: \n");
        printf("\tQuiet: %s\n", arguments->quiet ? "yes" : "no");
        printf("\tVerbose: %s\n", arguments->verbose ? "yes" : "no");
        printf("MLP architecture options:\n");
        printf("\tDesired file: %s\n", arguments->desired_file != NULL ? arguments->desired_file : "NULL");
        printf("\tMax error: %f\n", arguments->max_error);
        printf("\tGeneralization: %f\n", arguments->generalization);
        printf("\tHidden neurons: %i\n", arguments->n_hidden);
        printf("\tInput file: %s\n", arguments->input_file != NULL? arguments->input_file : "NULL");
        printf("\tMax epochs: %i\n", arguments->m_epoch);
        printf("\tPrefix: %s\n", arguments->prefix != NULL ? arguments->prefix : "none");
        printf("\tRandomize: %s\n", arguments->randomize ? "yes" : "no");
        printf("\tShow Statistics: %s\n", arguments->statistics ? "yes" : "no");
        printf("\tTest file: %s\n", arguments->test_file != NULL ? arguments->test_file : "NULL");
        printf("\tEta: %f\n", arguments->eta);
        printf("\tAlpha: %f\n", arguments->alpha);
}
