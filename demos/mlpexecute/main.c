// vim: set ts=8
#include <stdio.h>
#include <math.h>
#include "mlp.h"
#include "mlp_topology.h"
#include "mlp_arch.h"
#include "mlpexecute_argp.h"


float 
hyperbolic_tangent(float v);


float 
f_ativacao(float v);


/*
* load matrices
* return 0 in case of success, 1 otherwise
*/
int 
load_matrices(arguments_t *arguments, matrix_t** W1, matrix_t** W2,
                matrix_t** B1, matrix_t** B2, matrix_t** X);

int 
test_matrices(matrix_t* W1, matrix_t* W2,
                matrix_t* B1, matrix_t *B2, matrix_t *X);

void
free_matrices(matrix_t* W1, matrix_t* W2,
                matrix_t* B1, matrix_t *B2, matrix_t *X);

int 
main (int argc, char **argv)
{
        size_t n_input;
        size_t n_hidden;
        size_t n_output;

        matrix_t *W1;
        matrix_t *W2;
        matrix_t *B1;
        matrix_t *B2;
        matrix_t *X;
        matrix_t *O;

        // parse arguments
        arguments_t *arguments;
        arguments = mlpexecute_argp_parse(argc, argv, 0, 0);
 
        mlp_topology_t *topology;
        mlp_arch_t *arch;
        
        // for quiet executions
        if (arguments->quiet == 1) {
                fclose(stdout);
                fclose(stderr);
        }

        // show options if in verbose mode
        if (arguments->verbose == 1) {
                mlpexecute_argp_print(arguments);
        }

        if (load_matrices(arguments, &W1, &W2, &B1, &B2, &X)) {
                fprintf(stderr, "<mlp_execute> Erro ao carregar matrizes (%p, %p, %p, %p, %p).\n", 
                        W1, W2, B1, B2, X);
                free_matrices(W1, W2, B1, B2, X);
                return 1;
        }
        
        if (test_matrices(W1, W2, B1, B2, X)) {
                fprintf(stderr, "<mlp_execute> Dimensoes inapropriadas.\n");
                free_matrices(W1, W2, B1, B2, X);
                return 1;
        }
/*        printf("W1:\n"); print_matrix(W1);*/
/*        printf("W2:\n"); print_matrix(W2);*/
/*        printf("B1:\n"); print_matrix(B1);*/
/*        printf("B2:\n"); print_matrix(B2);*/
/*        printf("X:\n"); print_matrix(X);*/
        // Get and Test the sizes of matrixes
        n_input  = get_cols_matrix(W1);
        n_hidden = get_rows_matrix(W1);
        n_output = get_rows_matrix(W2);
        
        
        topology = create_mlp_topology(n_input,n_hidden, n_output, f_ativacao);
        arch = create_mlp_arch();
        mlp_arch_set(arch, W1, W2, B1, B2);


/*        // Execute mlp*/
        O = execute_mlp(topology, arch, X);
        FILE *saida = stdout;
        
        
        if (arguments->output_file != NULL) {
                saida = fopen(arguments->output_file,"w");
                if (saida == NULL) {
                        fprintf(stderr, "Erro ao abrir aquivo %s.\n", arguments->output_file);
                        saida = stdout;
                }
        }
        fprint_matrix(saida, O);

        free_matrices(W1, W2, B1, B2, X);
        return 0;
}

float 
hyperbolic_tangent(float v)
{
        // hint
        // Haykin 1994, pg 160
        return (3.432/(1+exp(-0.6666667*v))) - 1.716;
}


float f_ativacao(float v)
{
   return (1.-exp(-v))/(1.+exp(-v));
}


int 
load_matrices(arguments_t *arguments, matrix_t** W1, matrix_t** W2,
                matrix_t** B1, matrix_t** B2, matrix_t** X)
{
        // Load data from files
        (*W1) = load_matrix(arguments->w1_file);
        (*W2) = load_matrix(arguments->w2_file);
        (*B1) = load_matrix(arguments->b1_file);
        (*B2) = load_matrix(arguments->b2_file);
        (*X)  = load_matrix(arguments->input_file);
        
        if (W1 == NULL || W2 == NULL || B1 == NULL || B2 == NULL || X==NULL) {
                return 1;
        }
        return 0;
}


int 
test_matrices(matrix_t* W1, matrix_t* W2,
                matrix_t* B1, matrix_t *B2, matrix_t *X)
{
        int err = 0;
        size_t n_input  = get_cols_matrix(W1);
        size_t n_hidden = get_rows_matrix(W1);
        size_t n_output = get_rows_matrix(W2);

        // W1 e X
        if (n_input != get_rows_matrix(X)) {
                fprintf(stderr, "<mlp_execute> W1 e X tem tamanhos inapropriados.\n");
                ++err;  
        }

        // W1 e W2
        if (n_hidden != get_cols_matrix(W2) ) {
                fprintf(stderr, "<mlp_execute> W1 e W2 tem tamanhos inapropriados.\n");
                ++err;
        }
        
        // W1 e B1
        if (n_hidden != get_rows_matrix(B1) ) {
                fprintf(stderr, "<mlp_execute> W1 e B1 em tamanhos inapropriados.\n");
                ++err;
        }
        
        // W2 e B2
        if (n_output != get_rows_matrix(B2) ) {
                fprintf(stderr, "<mlp_execute> W2 e B2 em tamanhos inapropriados.\n");
                ++err;
        }
        
        return err;
}

void
free_matrices(matrix_t* W1, matrix_t* W2,
                matrix_t* B1, matrix_t *B2, matrix_t *X)
{
        destroy_matrix(W1);
        destroy_matrix(W2);
        destroy_matrix(B1);
        destroy_matrix(B2);
        destroy_matrix(X);

}
