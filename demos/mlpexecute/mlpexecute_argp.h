// vim: set ts=8
#ifndef __MLPARGUMENTS_H__
#define __MLPARGUMENTS_H__

#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <string.h> //atoi atof

typedef struct arguments {
        int quiet;
        int verbose;
        // mlpexecute options:
        char *w1_file;
        char *w2_file;
        char *b1_file;
        char *b2_file;
        char *input_file;
        char *output_file;
}arguments_t;

arguments_t *
mlpexecute_argp_parse(int argc, char **argv, unsigned int flags, int *arg_index);

void
mlpexecute_argp_print(arguments_t *arguments);

#endif /* __MLPARGUMENTS_H__ */

