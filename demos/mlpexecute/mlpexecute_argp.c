// vim: set ts=8
#include "mlpexecute_argp.h"


/*  *********************************************************
        Variaveis usadas para o help e parser do mlpexecute 
    *********************************************************  */


const char *argp_program_version = "mlpexecute 0.2";
const char *argp_program_bug_address ="<mlpexecute-bug@vconrado.com>";

// documentacao
static char doc[] = 
        "Execute a MLP Neural Network.\
        \v\
        Example:\n \
        mlpexecute W1.mat W2.mat B1.mat B2.mat input_file.mat [-o output_file.mat]";

// argumentos 
static char args_doc[] = "W1.mat W2.mat B1.mat B2.mat input_file";

// argumentos que sao aceitados
static struct argp_option options[] = {
        {"verbose",     'v',    0,              0,              "Produce verbose output (debug)" },
        {"debug",       0,      0,              OPTION_ALIAS},
        {"quiet",       'q',    0,              0,              "Don't produce any output" },
        {0,             0,      0,              0,              "MLP architecture options:"},
        {"output",    'o',    "STRING",       0,              "Output file"},
        { 0 }
};

// faz o tratamento de cada argumento passado
static error_t
mlpexecute_parse_opt (int key, char *arg, struct argp_state *state)
{
        // pega a estrutura para atualizar
        struct arguments *arguments = state->input;

        switch (key) {
                case 'q':
                        arguments->quiet = 1;
                        break;
                case 'v':
                        arguments->verbose = 1;
                        break;
                case 'o':
                        arguments->output_file = arg;
                        break;
                case ARGP_KEY_ARG:
                        switch (state->arg_num ) {
                                case 0:
                                        arguments->w1_file = arg;
                                        break;
                                case 1:
                                        arguments->w2_file = arg;
                                        break;
                                case 2:
                                        arguments->b1_file = arg;
                                        break;
                                case 3:
                                        arguments->b2_file = arg;
                                        break;
                                case 4: 
                                        arguments->input_file = arg;
                                        break;
                                default:
                                        // excesso de argumentos
                                        argp_usage(state);
                        }
                        break;
                case ARGP_KEY_END:
                        if (state->arg_num < 5) {
                                argp_usage(state);
                        }
                        break;
                default:
                        return ARGP_ERR_UNKNOWN;
        
        }
        return 0;
}


// funcao que faz o tratamento e o registro para o parser
arguments_t *
mlpexecute_argp_parse(int argc, char **argv, unsigned int flags, int *arg_index)
{
        // variavel enviada ao parser
        static struct argp argp = { options, mlpexecute_parse_opt, args_doc, doc };

        arguments_t *arguments = (arguments_t *) malloc(sizeof(arguments_t));

        /* Default values. */
        arguments->quiet = 0;
        arguments->verbose = 0;
        // mlp architecture options:
        arguments->w1_file = NULL;
        arguments->w2_file = NULL;
        arguments->b1_file = NULL;
        arguments->b2_file = NULL;
        arguments->input_file = NULL;
        arguments->output_file = NULL;
        // call parser
        argp_parse (&argp, argc, argv, flags, arg_index, arguments);

        return arguments;
}


void
mlpexecute_argp_print(arguments_t *arguments)
{
        printf("Global options: \n");
        printf("\tQuiet: %s\n", arguments->quiet ? "yes" : "no");
        printf("\tVerbose: %s\n", arguments->verbose ? "yes" : "no");
        printf("MLP architecture options:\n");
        printf("\tW1: %s\n", arguments->w1_file != NULL ? arguments->w1_file : "NULL");
        printf("\tW2: %s\n", arguments->w2_file != NULL ? arguments->w2_file : "NULL");
        printf("\tB1: %s\n", arguments->b1_file != NULL ? arguments->b1_file : "NULL");
        printf("\tB2: %s\n", arguments->b2_file != NULL ? arguments->b2_file : "NULL");
        printf("\tInput file: %s\n", arguments->input_file != NULL? arguments->input_file : "NULL");
        printf("\tOutput file: %s\n", arguments->output_file != NULL? arguments->output_file : "stdout");
}
