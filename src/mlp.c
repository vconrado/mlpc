// vim: set ts=8
#include "mlp.h"


mlp_stats_t*
create_mlp_stats()
{
        mlp_stats_t *stats;
        stats = (mlp_stats_t*) malloc(sizeof(mlp_stats_t));
        stats->epochs = 0;
        stats->error = 0.;
        stats->m_error = NULL;
        return stats;
}

void
destroy_mlp_stats(mlp_stats_t *stats)
{
        free(stats);
}

mlp_training_param_t *
create_mlp_training_param(matrix_t *X, matrix_t *D, matrix_t *T, matrix_t *DT, float max_error, 
                                float max_epochs, float eta, float alpha, int seed)
{
        mlp_training_param_t *parameter;
        parameter = (mlp_training_param_t*) malloc(sizeof(mlp_training_param_t));
        parameter->X = X;
        parameter->D = D;
        parameter->T = T;
        parameter->DT = DT;
        parameter->max_error = max_error;
        parameter->max_epochs = max_epochs;
        parameter->eta = eta;
        parameter->alpha = alpha;
        parameter->seed = seed;
        
        return parameter;
}

void
destroy_mlp_training_param(mlp_training_param_t *parameter)
{
        free(parameter);
}


matrix_t *
execute_mlp(mlp_topology_t *topology, mlp_arch_t *architecture, matrix_t *X)
{
        size_t ipt;
        size_t r, c;
        float u;
        size_t n_pattern;
        matrix_t *Y1;
        matrix_t *Y2;
        if (topology == NULL || architecture == NULL || X==NULL) {
                fprintf(stderr,"<execute_mlp> null arguments. (%p, %p, %p)\n", topology, architecture, X);
                return NULL;
        }
        
        n_pattern = get_cols_matrix(X);
        
        Y1 = create_matrix(topology->n_hidden, 1);
        Y2 = create_matrix(topology->n_output, n_pattern);
        
        /* For each input */
        for (ipt=0; ipt < n_pattern; ipt++){
                /* Calculates neurone internal activity of hidden layer */
                for (r = 0; r < topology->n_hidden; r++) {
                        u = 1.0 * architecture->B1->storage[r][0];
                        for (c = 0; c < topology->n_input; c++) {
                                u = u + architecture->W1->storage[r][c] * X->storage[c][ipt];
                        }
                        /* Computes the activation function */
                        Y1->storage[r][0] = topology->activation_func(u);
                }
                /* Calculates neurone internal activity of output layer */
                for (r = 0; r < topology->n_output; r++) {
                        u = 1.0  * architecture->B2->storage[r][0];
                        for (c = 0; c < topology->n_hidden; c++) {
                                u = u + architecture->W2->storage[r][c] * Y1->storage[c][0];
                        }
                        /* calcula funcao de ativacao */
                        Y2->storage[r][ipt] = topology->activation_func(u);
                }
        }
        destroy_matrix(Y1);
        return Y2;
}


// traine mlp without generalization
mlp_arch_t *
training_mlp_basic(mlp_topology_t *topology, mlp_training_param_t *parameters, mlp_stats_t *stats)
{
        size_t ep, ipt;
        size_t r, c;
        size_t n_input = topology->n_input;
        size_t n_hidden = topology->n_hidden;
        size_t n_output = topology->n_output;
        size_t n_pattern;
        size_t max_epochs = parameters->max_epochs;
        float max_error = parameters->max_error;
        float u;
        float error;
        float acc_error;
        float alpha = parameters->alpha; // Momentum
        float eta = parameters->eta; // Learn Rate
        //float dw1, db1, dw2, db2;
        //float bias_dif;
        matrix_t *X = parameters->X;
        matrix_t *D = parameters->D;
        
        
        printf("Erro maximo %.10f\n", max_error);
        
        // Testando dados
        if (X == NULL || D == NULL) {
                fprintf(stderr,"<training_mlp_basic> null arguments. (%p,%p)\n", X, D);
                return NULL;
        }

        n_pattern = get_cols_matrix(X);

        if (n_pattern != get_cols_matrix(D)) {
                fprintf(stderr,"<training_mlp_basic> dimensions problems.\n");
                return NULL;
        }

        // creating temporary matrices
        matrix_t *DW1A = create_matrix(n_hidden, n_input);
        matrix_t *DB1A = create_matrix(n_hidden, 1);
        matrix_t *DW2A = create_matrix(n_output, n_hidden);
        matrix_t *DB2A = create_matrix(n_output, 1);

        matrix_t *Y1 = create_matrix(n_hidden, 1);
        matrix_t *Y2 = create_matrix(n_output, 1);
        matrix_t *GS = create_matrix(n_output, 1);
        matrix_t *G1 = create_matrix(n_hidden, 1);

        /* create MLP Architecture */
        mlp_arch_t *architecture = create_mlp_arch();

        // creatting weights matrices and bias matrices
        matrix_t *W1 = create_matrix(n_hidden, n_input);
        matrix_t *B1 = create_matrix(n_hidden, 1);
        matrix_t *W2 = create_matrix(n_output, n_hidden);
        matrix_t *B2 = create_matrix(n_output, 1);

        /* filling weights matrices and bias matrices */
        
        srand(parameters->seed);
        
        fill_matrix_randonly(&W1);
        fill_matrix_zero(&DW1A);
	printf("W1\n");
	print_matrix(W1);	
        
	fill_matrix_randonly(&W2);
        fill_matrix_zero(&DW2A);
	printf("W2\n");
	print_matrix(W2);	
        
	fill_matrix_randonly(&B1);
        fill_matrix_zero(&DB1A);
	printf("B1\n");
	print_matrix(B1);

        fill_matrix_randonly(&B2);
        fill_matrix_zero(&DB2A);
   	printf("B2\n");
	print_matrix(B2);   

        printf("Starting training...\n");

        /* For each epoch */
        for (ep = 0; ep < max_epochs; ep++) {
                
                acc_error = 0.;
                //printf("################ EPOCA %zu ##############\n", ep);
                
                /* For each input */
                for (ipt=0; ipt < n_pattern; ipt++){
                	/*printf("################ PADRAO %zu ##############\n",ipt);
	                printf("B1\n");
        	        print_matrix(B1);
	                getchar();
        	        printf("W1\n");
	                print_matrix(W1);
	                getchar();
	                printf("B2\n");
	                print_matrix(B2);
	                getchar();
	                printf("W2\n");
	                print_matrix(W2);
	                getchar();*/
                        /* Calculates neurone internal activity of hidden layer */
                        for (r = 0; r < n_hidden; r++){

                                u = 1.0 * B1->storage[r][0];
/*                                printf("u: %d %f \n",__LINE__, u);*/
                                for (c = 0; c < n_input; c++) {
                                        u = u + W1->storage[r][c] * X->storage[c][ipt];
/*                                        printf("u: %d %f \n",__LINE__, u);  */
                                }
                                /* Computes the activation function */
/*                                printf("u: %d %f \n",__LINE__, u);*/
                                Y1->storage[r][0] = topology->activation_func(u);
                        }
                        /*printf("Y1\n");
                        print_matrix(Y1);
                        getchar();*/
                        
                        /* Calculates neurone internal activity of output layer */
                        for (r = 0; r < n_output; r++) {
                                u = 1.0  * B2->storage[r][0];
/*                                printf("u: %d %f \n",__LINE__, u);*/
                                for (c = 0; c < n_hidden; c++) {
                                        u = u + W2->storage[r][c] * Y1->storage[c][0];
/*                                        printf("u: %d %f \n",__LINE__, u);*/
                                }
/*                                printf("u: %d %f \n",__LINE__, u);*/
                                /* calcula funcao de ativacao */
                                Y2->storage[r][0] = topology->activation_func(u);
                        }
                        
                        /*printf("Y2\n");
                        print_matrix(Y2);
                        getchar();*/
                        
                        /* Calcula o erro e o gradiente da camada de saida */
                        for (r = 0; r < n_output; r++) {
                                error = (D->storage[r][ipt] - Y2->storage[r][0]);
/*                                printf("error %f\n", error);*/
                                acc_error = acc_error + (error > 0 ? error : - error);
                                // GS == SignalError (camada de saida)
                                GS->storage[r][0] = error * Y2->storage[r][0] * (1.0 - Y2->storage[r][0]);
                        }
                        
                        /*printf("GS\n");
                        print_matrix(GS);
                        getchar();*/

                        
                        
                        /* Calculando Gradiente da camada escondida */
                        for (r = 0; r < n_hidden; r++) {
                                u = 0.;
                                for (c = 0; c < n_output; c++) {
                                        u = u + W2->storage[c][r] * GS->storage[c][0];
                                }
                                // SignalError (camada escondida)
                                G1->storage[r][0] = Y1->storage[r][0] * (1.0 - Y1->storage[r][0]) * u;
                        }
                        
                        /*printf("G1\n");
                        print_matrix(G1);
                        getchar();*/

                        /* Deltas e atualiza Ws e Bs*/
                        
                        // camada de escondida
                        for (r = 0; r < n_hidden; r++) {
                        
                            // atualiza os bias
                            DB1A->storage[r][0] = eta*G1->storage[r][0]  + alpha * DB1A->storage[r][0];
                            B1->storage[r][0] = B1->storage[r][0] + DB1A->storage[r][0];
                        
                                
                            for(c=0; c<n_input; c++){
                                DW1A->storage[r][c] =  eta * G1->storage[r][0] * X->storage[r][ipt] + alpha * DW1A->storage[r][c];              
                                W1->storage[r][c] = W1->storage[r][c] + DW1A->storage[r][c];
                            }  
                        }
			/*printf("DW1A\n");
			print_matrix(DW1A);*/
                        // camada saida
                        for (r = 0; r < n_output; r++) {
                                // bias
                                DB2A->storage[r][0] = eta*GS->storage[r][0] + alpha * DB2A->storage[r][0];
				//printf("B2[%zu] = %lf + %lf (%lf * %lf ) = %lf\n", r, B2->storage[r][0],  DB2A->storage[r][0], eta, GS->storage[r][0], B2->storage[r][0] + DB2A->storage[r][0]  );
                                B2->storage[r][0] = B2->storage[r][0] + DB2A->storage[r][0];  
                            
                            for (c = 0; c < n_hidden; c++) {
                                    DW2A->storage[r][c] = eta * GS->storage[r][0] * Y1->storage[c][0] + alpha * DW2A->storage[r][c] ;
                                    W2->storage[r][c] = W2->storage[r][c] + DW2A->storage[r][c];
                            }
                               
                        }
			/*printf("DW2A\n");
			print_matrix(DW2A);*/
                } // end of an pattern
                acc_error = acc_error/n_pattern;
                /* Verifies the error target was reached */
                //printf("############ acc_error : %zu %f\n", ep, acc_error);

                if (acc_error <= max_error) {
                        /* Setting MLP Training Statistics */
                        if (stats != NULL) {
                                stats->epochs = ep;
                                stats->error = acc_error;
                        }
                        break;
                }
        } // end of an epoch
   
        destroy_matrix(DW1A);
        destroy_matrix(DB1A);
        destroy_matrix(DW2A);
        destroy_matrix(DB2A);

        
        mlp_arch_set(architecture, W1, W2, B1, B2);

        printf("Training completed.\n");
        
        return architecture;
}


// traine mlp with generalization
mlp_arch_t *
training_mlp(mlp_topology_t *topology, mlp_training_param_t *parameters, mlp_stats_t *stats)
{
        size_t ep, ipt;
        size_t r, c;
        size_t n_input = topology->n_input;
        size_t n_hidden = topology->n_hidden;
        size_t n_output = topology->n_output;
        size_t n_pattern;
        size_t n_test_pattern;
        size_t max_epochs = parameters->max_epochs;
        float max_error = parameters->max_error;
        float u;
        float error;
        float acc_error;
        float alpha = parameters->alpha;
        float eta = parameters->eta;
        float dw1, db1, dw2, db2;
        matrix_t *X = parameters->X;
        matrix_t *D = parameters->D;
        matrix_t *T = parameters->T;
        
        // verificando se existe T
        if (T == NULL) {
                fprintf(stderr, "<training_mlp> T matrix null.\n");
                printf("The Training matrix (T) is null, running basic trainning.\n");
                return training_mlp_basic(topology, parameters, stats);
        }
        
        // Testando dados
        if (X == NULL || D == NULL) {
                fprintf(stderr,"<training_mlp> null arguments. (%p,%p)\n", X, D);
                return NULL;
        }
        
        n_pattern = get_cols_matrix(X);
        n_test_pattern = get_cols_matrix(T);

        if (n_pattern != get_cols_matrix(D)) {
                fprintf(stderr,"<training_mlp> dimensions problems.\n");
                return NULL;
        }

        // creating temporary matrices
        matrix_t *DW1A = create_matrix(n_hidden, n_input);
        matrix_t *DB1A = create_matrix(n_hidden, 1);
        matrix_t *DW2A = create_matrix(n_output, n_hidden);
        matrix_t *DB2A = create_matrix(n_output, 1);

        matrix_t *Y1 = create_matrix(n_hidden, 1);
        matrix_t *Y2 = create_matrix(n_output, 1);
        matrix_t *GS = create_matrix(n_output, 1);
        matrix_t *G1 = create_matrix(n_hidden, 1);

        /* create MLP Architecture */
        mlp_arch_t *architecture = create_mlp_arch();

        // creatting weights matrices and bias matrices
        matrix_t *W1 = create_matrix(n_hidden, n_input);
        matrix_t *B1 = create_matrix(n_hidden, 1);
        matrix_t *W2 = create_matrix(n_output, n_hidden);
        matrix_t *B2 = create_matrix(n_output, 1);

        /* filling weights matrices and bias matrices */
        
        srand(parameters->seed);
        
        fill_matrix_randonly(&W1);
        fill_matrix_zero(&DW1A);

        fill_matrix_randonly(&W2);
        fill_matrix_zero(&DW2A);

        fill_matrix_randonly(&B1);
        fill_matrix_zero(&DB1A);

        fill_matrix_randonly(&B2);
        fill_matrix_zero(&DB2A);
   
   
        printf("Starting training...\n");

        /* For each epoch */
        for (ep = 0; ep < max_epochs; ep++) {
                
                /* For each input */
                for (ipt=0; ipt < n_pattern; ipt++){
                        /* Calculates neurone internal activity of hidden layer */
                        for (r = 0; r < n_hidden; r++){
                                u = 1.0 * B1->storage[r][0];
                                for (c = 0; c < n_input; c++) {
                                        u = u + W1->storage[r][c] * X->storage[c][ipt];
                                }
                                /* Computes the activation function */
                                Y1->storage[r][0] = topology->activation_func(u);
                        }
                        /* Calculates neurone internal activity of output layer */
                        for (r = 0; r < n_output; r++) {
                                u = 1.0  * B2->storage[r][0];
                                for (c = 0; c < n_hidden; c++) {
                                        u = u + W2->storage[r][c] * Y1->storage[c][0];
                                }
                                /* calcula funcao de ativacao */
                                Y2->storage[r][0] = topology->activation_func(u);
                        }
                        /* Calcula o erro e o gradiente da camada de saida */
                        for (r = 0; r < n_output; r++) {
                                error = (D->storage[r][ipt] - Y2->storage[r][0]);
                                GS->storage[r][0] = error * Y2->storage[r][0] * (1.0 - Y2->storage[r][0]);
                        }
                        /* Calculando Gradiente da camada escondida */
                        for (r = 0; r < n_hidden; r++) {
                                u = 0.;
                                for (c = 0; c < n_output; c++) {
                                        u = W2->storage[c][r] * GS->storage[c][0];
                                }
                                G1->storage[r][0] = Y1->storage[r][0] * (1.0 - Y1->storage[r][0]) * u;
                        }
                        /* Deltas e atualiza Ws e Bs*/
                        for (r = 0; r < n_hidden; r++) {
                                for(c=0; c<n_input; c++){
                                        dw1 = eta * G1->storage[r][0] * X->storage[c][ipt];
                                        W1->storage[r][c] = W1->storage[r][c] + dw1 + alpha * DW1A->storage[r][c];
                                        DW1A->storage[r][c] = dw1;
                                }
                                db1 = eta*G1->storage[r][0];
                                B1->storage[r][0] = B1->storage[r][0] - db1 - alpha * DB1A->storage[r][0];
                                DB1A->storage[r][0] = db1;
                        }
                        for (r = 0; r < n_output; r++) {
                                for (c = 0; c < n_hidden; c++) {
                                        dw2 = eta * GS->storage[r][0] * Y1->storage[c][0];
                                        W2->storage[r][c] = W2->storage[r][c] + dw2 + alpha*DW2A->storage[r][c];
                                        DW2A->storage[r][c] = dw2;
                                }
                                db2 = eta*GS->storage[r][0];
                                B2->storage[r][0] = B2->storage[r][0] - db2 - alpha*DB2A->storage[r][0];
                                DB2A->storage[r][0] = db2;
                        }
                } // end of an pattern
              
                // execute generalization
                acc_error = 0.;
                for (ipt = 0; ipt < n_test_pattern; ipt++) {
                        /* Calculates neurone internal activity of hidden layer */
                        for (r = 0; r < n_hidden; r++){
                                u = 1.0 * B1->storage[r][0];
                                for (c = 0; c < n_input; c++) {
                                        u = u + W1->storage[r][c] * T->storage[c][ipt];
                                }
                                /* Computes the activation function */
                                Y1->storage[r][0] = topology->activation_func(u);
                        }
                        /* Calculates neurone internal activity of output layer */
                        for (r = 0; r < n_output; r++) {
                                u = 1.0  * B2->storage[r][0];
                                for (c = 0; c < n_hidden; c++) {
                                        u = u + W2->storage[r][c] * Y1->storage[c][0];
                                }
                                /* calcula funcao de ativacao */
                                Y2->storage[r][0] = topology->activation_func(u);
                        }
                        /* Calcula o erro da camada de saida */
                        for (r = 0; r < n_output; r++) {
                                acc_error = acc_error + (D->storage[r][ipt] - Y2->storage[r][0]);
                        }
                }
                acc_error = (acc_error/n_output)/n_test_pattern;
                if (acc_error <= max_error ) {
                        break;
                }

        } // end of an epoch
   
        destroy_matrix(DW1A);
        destroy_matrix(DB1A);
        destroy_matrix(DW2A);
        destroy_matrix(DB2A);

        /* create MLP Training Statistics */
        if (stats != NULL) {
                stats->epochs = ep;
        }
        
        mlp_arch_set(architecture, W1, W2, B1, B2);
        
        return architecture;
}
void
mlp_training_param_print(mlp_training_param_t *parameter)
{
        fprintf(stdout,"MLP Parameter:\n");
        fprintf(stdout,"\t#X:\n");
        print_matrix(parameter->X);
        fprintf(stdout,"\t#D:\n");
        print_matrix(parameter->D);
        fprintf(stdout,"\t#T:\n");
        print_matrix(parameter->T);
        fprintf(stdout,"\t#DT:\n");
        print_matrix(parameter->DT);
        fprintf(stdout,"\t#Max error: %.10f\n", parameter->max_error);
        fprintf(stdout,"\t#Max epochs: %.10f\n", parameter->max_epochs);
        fprintf(stdout,"\t#Eta: %.10f\n", parameter->eta);
        fprintf(stdout,"\t#Alpha: %.10f\n", parameter->alpha);
        fprintf(stdout,"\t#Seed: %d\n", parameter->seed);
}

// print mlp stats
void
mlp_stats_print(mlp_stats_t *stats)
{
        if (stats != NULL) {
                fprintf(stdout,"Statistics:\n");
                fprintf(stdout,"\t#Total epochs: %zu\n",stats->epochs);
                fprintf(stdout,"\t#Max mean error: %.10f\n",stats->error);
                print_matrix(stats->m_error);
        } else {
                fprintf(stderr, "<mlp_stats_print> Null Pointer.\n");
        }
}
