#include "mlp_topology.h"


mlp_topology_t *
create_mlp_topology(size_t n_input, size_t n_hidden, size_t n_output,
                         float (*activation_func)(float) )
{
        mlp_topology_t *topology;
        topology = (mlp_topology_t*) malloc(sizeof(mlp_topology_t));

        mlp_topology_set(topology, n_input, n_hidden, n_output, activation_func);

        return topology;
}

void
mlp_topology_set(mlp_topology_t *topology, size_t n_input, size_t n_hidden, size_t n_output,
                         float (*activation_func)(float))
{
        topology->n_input = n_input;
        topology->n_hidden = n_hidden;
        topology->n_output = n_output;
        topology->activation_func = activation_func;
}


void
destroy_mlp_topology(mlp_topology_t *topology)
{
        if (topology != NULL) {
                free(topology);
        }
}

void
mlp_topology_print(mlp_topology_t *topology)
{
   fprintf_mlp_topology(stdout, topology);
}

void
fprintf_mlp_topology(FILE *fd, mlp_topology_t *topology)
{
        if (topology != NULL) {
                fprintf(fd, "MLP Topology:\n");
                fprintf(fd, "\t#Input: %zu neurons\n", topology->n_input);
                fprintf(fd, "\t#Hidden: %zu neurons\n", topology->n_hidden);
                fprintf(fd, "\t#Output: %zu neurons\n", topology->n_output);
                fprintf(fd, "\t#Activation function: %p\n", topology->activation_func);
        } else {
                fprintf(stderr, "<fprint_mlp_topology> null argument.\n");
        }
}
