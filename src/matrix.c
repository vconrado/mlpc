// vim: set ts=8
#include "matrix.h"


// cria matriz
matrix_t *
create_matrix(size_t rows, size_t cols){
        size_t i;
        matrix_t *N = (matrix_t*) malloc(sizeof(matrix_t));
        N->rows = rows;
        N->cols = cols;
        N->storage = (float**) malloc(sizeof(float*)*rows);
        for(i=0; i<rows; i++){
                N->storage[i] = (float*) malloc(sizeof(float)*cols);
        }
        return N;
}

// cria uma matriz com elementos aleatorios entre -1 e 1
void
fill_matrix_randonly(matrix_t **M){
        size_t r, c;
        size_t rows_m = get_rows_matrix(*M);
        size_t cols_m = get_cols_matrix(*M);
        #pragma omp for schedule (dynamic) private(r,c)
        for(r = 0; r<rows_m; r++){
                for(c = 0; c<cols_m; c++){
                        (*M)->storage[r][c] = ((float)rand()/(float)RAND_MAX)*2. - 1.;
                }
        }
}

// carrega com zero
void
fill_matrix_zero(matrix_t **M){
        size_t r, c;
        size_t rows_m = get_rows_matrix(*M);
        size_t cols_m = get_cols_matrix(*M);
        #pragma omp for schedule (dynamic)  private(r,c)
        for(r = 0; r<rows_m; r++){
                for(c = 0; c<cols_m; c++){
                        (*M)->storage[r][c] = 0.;
                }
        }
}



// carrega matriz de um arquivo no formato do octave
matrix_t *
load_matrix(const char *file_name){
        matrix_t *N;
        FILE *fp;
        size_t rows;
        size_t cols;
        // abre arquivo
        fp = fopen(file_name,"r");
        if(fp == NULL){
                fprintf(stderr, "<load_matrix> Nao foi possivel abrir %s.\n", file_name);
                return NULL;
        }
        // remove os comentarios iniciais
        readfile_skip_comments(fp);
        // pega total de colunas e linhas
        cols = readfile_get_cols(fp);
        rows = readfile_get_rows(fp);
        if(rows < 1 || cols < 1){
                fprintf(stderr,"<load_matrix> Erro ao obter tamanho da matriz.\n");
                return NULL;
        }
        // cria matriz
        N = create_matrix(rows, cols);
        // le matriz e armazena em um float** previamente alocado
        if(readfile_read_set(fp, rows, cols, &(N->storage))){
                fprintf(stderr,"<load_matrix> Erro ao carregar matrix.\n");
                destroy_matrix(N);
                N = NULL;
        }
        fclose(fp);
        return N;
}

int 
save_matrix(const matrix_t *M, const char *file_name){
        FILE *fp;
        fp = fopen(file_name,"w");
        if(file_name == NULL){
                fprintf(stderr, "<save_matrix> Nao foi possivel gravar matrix em %s.\n", file_name);
                return 1;
        }
        fprint_matrix(fp, M);
        return 0;
}


void
destroy_matrix(matrix_t *M){
        if(M!=NULL){
                free(M->storage);
                free(M);
        }
}

void 
print_matrix(const matrix_t *M){
        fprint_matrix(stdout, M);
}

void 
fprint_matrix(FILE *fp, const matrix_t *M)
{
        size_t r,c;
        if(M!=NULL) {
                for(r = 0; r<M->rows; r++){
                        fprintf(fp,"\t");
                        for(c = 0; c<M->cols; c++){
                                fprintf(fp, "%0.10f  ",M->storage[r][c]);
                        }
                        fprintf(fp, "\n");
                }
        } else {
                fprintf(stderr, "\t<print_matrix> Matrix nula.\n");
        }
}


size_t 
get_cols_matrix(const matrix_t *M){
        return M->cols;
}

size_t 
get_rows_matrix(const matrix_t *M){
        return M->rows;
}

