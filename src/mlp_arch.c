#include "mlp_arch.h"

mlp_arch_t *
create_mlp_arch()
{
        mlp_arch_t *arch;
        arch = (mlp_arch_t*) malloc(sizeof(mlp_arch_t));
        
        mlp_arch_set(arch, NULL, NULL, NULL, NULL);
        
        return arch;
}

void
mlp_arch_set(mlp_arch_t *arch, matrix_t *W1, matrix_t *W2,
                        matrix_t *B1, matrix_t *B2)
{
        arch->W1 = W1;
        arch->W2 = W2;
        
        arch->B1 = B1;
        arch->B2 = B2;
}

void 
destroy_mlp_arch(mlp_arch_t *arch)
{
        if (arch != NULL) {
                free(arch);
        }
}

// save in the format prefix_W[1,2].mat, prefix_B[1,2].mat
void 
save_mlp_arch(char *prefix, mlp_arch_t *arch)
{
        char *file_name;
        char *myprefix;
        size_t size_prefix = 0;
        
        if (prefix != NULL) {
                size_prefix = strlen(prefix);
        }

        if (size_prefix > 0) {
                myprefix = (char*) malloc(sizeof(char)*(size_prefix+2));
                sprintf(myprefix, "%s_", prefix);
        } else {
                 myprefix = (char*) malloc(sizeof(char));
                 myprefix[0] = '\0';
        }

        file_name = (char*) malloc(sizeof(char)*(size_prefix + 8));

        // save W1
        sprintf(file_name, "%sW1.mat", myprefix);
        save_matrix(arch->W1, file_name);
        
        // save W2
        sprintf(file_name, "%sW2.mat", myprefix);
        save_matrix(arch->W2, file_name);
        
        // save B1
        sprintf(file_name, "%sB1.mat", myprefix);
        save_matrix(arch->B1, file_name);
        
        // save B2
        sprintf(file_name, "%sB2.mat", myprefix);
        save_matrix(arch->B2, file_name);

        free(file_name);
        
        free(myprefix);

}

void
fprint_mlp_arch(FILE *fd, mlp_arch_t *arch){
        if (arch != NULL) {
                fprintf(fd, "MLP Architecture:\n");

                // W1
                fprintf(fd, "W1:\n");
                fprint_matrix(fd, arch->W1);

                // W2
                fprintf(fd, "W2:\n");
                fprint_matrix(fd, arch->W2);

                // B1
                fprintf(fd, "B1:\n");
                fprint_matrix(fd, arch->B1);

                // B2
                fprintf(fd, "B2:\n");
                fprint_matrix(fd, arch->B2);
        } else {
                fprintf(stderr, "<fprint_mpl_arch> null argument.\n");
        }

}
