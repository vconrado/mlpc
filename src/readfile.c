#include "readfile.h"

size_t
readfile_skip_comments(FILE *fp){
   char c;
   size_t lines = 0;
   if(fp == NULL){
      fprintf(stderr,"<file_skip_comments> fp nulo.\n");
      return lines;
   }
   while(1){
      c = fgetc(fp);
      if(readfile_iscomment(c)){
         // vai ate o fim da linha
         lines++;
         for(c=fgetc(fp); c!='\n' && c!=EOF; c=fgetc(fp))
            ;
      }else{
         // volta um caracter 
         fseek(fp,-1, SEEK_CUR);
         break;
      }
   }
   return lines;
}

size_t 
readfile_get_cols(FILE *fp){
   char c;
   int s = 1;
   size_t cols = 0;
   fpos_t position;
   // pega a posicao atual
   fgetpos(fp, &position);
   for(c=fgetc(fp); c!='\n' && c!=EOF; c=fgetc(fp)){
      if(readfile_isspace(c)){
         s = 1;
      }else if(s == 1){
         cols++;
         s = 0;
      }
   }
   // volta a posicao inical
   fsetpos(fp, &position);
   return cols;
}

size_t
readfile_get_rows(FILE *fp){
   size_t rows = 0;
   char c;
   fpos_t position;
   // pega a posicao atual
   fgetpos(fp, &position);
   for(c=fgetc(fp); c!=EOF; c=fgetc(fp)){
      if(c=='\n'){
         rows++;
      }
   }
    // volta a posicao inical
   fsetpos(fp, &position);
   return rows;
}

/* 
   Le o arquivo e retorna uma matriz linhaxcoluna com os dados
   retorno:
   0: ok
   1: faltando colunas
*/

int
readfile_read_set(FILE *fp, size_t rows, size_t cols, float ***storage){
   size_t r, c;
   float value;
   for(r = 0; r<rows; r++){
      for(c = 0; c<cols; c++){
         if(fscanf(fp, "%f", &value)){
            (*storage)[r][c] = value;
         }else{
            fprintf(stderr,"<readfile_read_set> Erro ao ler elemento (%zu,%zu) da matrix.\n",r,c);
            return 1;
         }
      }
   }
   return 0;
}


