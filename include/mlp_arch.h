#ifndef __MLP_ARCH_H__
#define __MLP_ARCH_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"

// struct of mlp architecture
typedef struct mlp_arch{
        matrix_t *W1, *W2;                      // weights
        matrix_t *B1, *B2;                      // bias
}mlp_arch_t;

mlp_arch_t *
create_mlp_arch();

void
mlp_arch_set(mlp_arch_t *arch, matrix_t *W1, matrix_t *W2,
                        matrix_t *B1, matrix_t *B2);

void 
destroy_mlp_arch(mlp_arch_t *arch);

// save in the format prefix_W[1,2].mat, prefix_B[1,2].mat
void 
save_mlp_arch(char *prefix, mlp_arch_t *arch);

void
fprint_mlp_arch(FILE *fd, mlp_arch_t *arch);

#endif /* __MLP_ARCH_H__ */
