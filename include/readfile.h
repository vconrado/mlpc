#ifndef __READ_FILE_H__
#define __READ_FILE_H__

#include <stdio.h>
#include <stdlib.h>

static __inline__ int 
readfile_iscomment(char c){ return (c=='#'); }

static __inline__ int
readfile_isspace(char c){ return (c==' ' || c=='\t'); }

// retorna o total de linhas comentadas
size_t
readfile_skip_comments(FILE *fp);

size_t 
readfile_get_cols(FILE *fp);

size_t
readfile_get_rows(FILE *fp);

/* 
   Le o arquivo e retorna uma matriz linhaxcoluna com os dados
   return:
   0: ok
   1: faltando colunas
*/

int
readfile_read_set(FILE *fp, size_t rows, size_t cols, float ***storage);

#endif /* __READ_FILE_H__ */
