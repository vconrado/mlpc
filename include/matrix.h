#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "readfile.h"

typedef struct{
   size_t rows;
   size_t cols;
   float **storage;
}matrix_t;


// cria matriz
matrix_t *
create_matrix(size_t rows, size_t cols);

// preenche uma matriz com elementos aleatorios entre 0 e 1
void
fill_matrix_randonly(matrix_t **M);

// carrega com zero
void
fill_matrix_zero(matrix_t **M);

// carrega matriz de um arquivo no formato L C\n dados
matrix_t *
load_matrix(const char *file_name);

int 
save_matrix(const matrix_t *M, const char *file_name);

void
destroy_matrix(matrix_t *M);

void 
print_matrix(const matrix_t *M);

void 
fprint_matrix(FILE *fp, const matrix_t *M);

size_t 
get_cols_matrix(const matrix_t *M);

size_t 
get_rows_matrix(const matrix_t *M);

//static __inline__ float
//get_elem_matrix(const matrix_t *M, size_t row, size_t col){ return M->storage[row][col];}



#endif /* __MATRIX_H__ */
