// vim: set ts=8
#ifndef __MLP_H__
#define __MLP_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "mlp_defines.h"
#include "mlp_topology.h"
#include "mlp_arch.h"
#include "matrix.h"


// struct of mlp statistics
typedef struct mlp_stats{
        size_t epochs;                          // total of epochs used in the traning process
        float error;                            // final mean absolute error
        matrix_t *m_error;                      // mean absolute error of each epoch
}mlp_stats_t;

typedef struct mlp_training_param{
        matrix_t *X;    // training set
        matrix_t *D;    // desired value for training set
        matrix_t *T;    // test set
        matrix_t *DT;   // desired values for test set
        float max_error;
        float max_epochs;
        float eta;
        float alpha;
        int seed;
}mlp_training_param_t;


mlp_stats_t*
create_mlp_stats();

void
destroy_mlp_stats(mlp_stats_t *stats);

mlp_training_param_t *
create_mlp_training_param(matrix_t *X, matrix_t *D, matrix_t *T, matrix_t *DT, float max_error, 
                                float max_epochs, float eta, float alpha, int seed);

void
destroy_mlp_training_param(mlp_training_param_t *parameter);

matrix_t *
execute_mlp(mlp_topology_t *topology, mlp_arch_t *architecture, matrix_t *X);

// traine mlp without generalization
mlp_arch_t *
training_mlp_basic(mlp_topology_t *topology, mlp_training_param_t *parameters, mlp_stats_t *stats);


// traine mlp with generalization
mlp_arch_t *
training_mlp(mlp_topology_t *topology, mlp_training_param_t *parameters, mlp_stats_t *stats);

// print mlp stats
void
mlp_stats_print(mlp_stats_t *stats);

// print mlp parameter
void
mlp_training_param_print(mlp_training_param_t *parameter);

#endif /* __MLP_H__ */
