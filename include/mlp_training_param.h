#ifndef __MLP_TRAINING_PARAM_H__
#define __MLP_TRAINING_PARAM_H__

struct mlp_training_param;
typedef struct mlp_training_param mlp_training_param_t;

mlp_training_param_t *
create_mlp_training_param(size_t max_epochs, float eta, float alpha, float max_error,
                                matrix_t *X, matrix_t *D, matrix_t *T);
                                


#endif /* __MLP_TRAINING_PARAM_H__ */
