// vim: set ts=8
#ifndef __MLP_TOPOLOGY_H__
#define __MLP_TOPOLOGY_H__

#include <stdio.h>
#include <stdlib.h>

// struct of mlp topology
typedef struct mlp_topology{
        size_t n_input;                         // number of neurons in the input layer
        size_t n_hidden;                        // number of neurons in the hidden layer
        size_t n_output;                        // number of neurons int he output layer
        float (*activation_func)(float);    // pointer to activation function
}mlp_topology_t;

mlp_topology_t *
create_mlp_topology(size_t n_input, size_t n_hidden, size_t n_output,
                         float (*activation_func)(float));

void
mlp_topology_set(mlp_topology_t *topology, size_t n_input, size_t n_hidden, size_t n_output,
                         float (*activation_func)(float));

void
destroy_mlp_topology(mlp_topology_t *topology);

void
mlp_topology_print(mlp_topology_t *topology);

void
fprintf_mlp_topology(FILE *fd, mlp_topology_t *topology);

#endif /* __MLP_TOPOLOGY_H__ */
